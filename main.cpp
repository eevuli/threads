// Ryhmä 13:
// Aleksi Olavi Martikainen, 240399, aleksi.o.martikainen@student.tut.fi
// Eeva Ylimäki, 246522, eeva.ylimaki@student.tut.fi
// Mikael Korpimaa, 240046, mikael.korpima@student.tut.fi


//Avaa heti useita säikeitä jotka kaikki suorittavat
// detect()(suojattu mutexilla) ja itenName() funktioita itsenäisesti.
// En ole tosin ihan varma, miten lukitseminen toimii tuonne ulkopuolelle
// joten vaihtoehto 2 voi olla helpompi. Ehkä vain 5% nopeampi kuin
// kurssin tarjoama sarjallinen ratkaisu, jokin vissiin kusee :/


#include "rinn2017.h"
#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <stdint.h>

// montako näytettä käsitellään ennen pysähtymistä
const int NAYTTEITA = 1000;
// montako säiettä avataan
const unsigned int SAIKEITA = 10;

// luodaan nollaksi alustetut laskurit
// kolmelle laskettavalle säteilytyypille
typedef uint64_t Laskuri_t;
struct {
    Laskuri_t alpha;
    Laskuri_t beta;
    Laskuri_t gamma;
} laskurit = { 0, 0, 0 };


// rinn2017 detectorin näytteen mukaisesti
// lisätään kyseisen säteilyn laskuria
void kirjaa_nayte( rinn2017::DetectorData_t nayte, std::mutex* mutex )
{
    auto nimi = rinn2017::itemName( nayte );
    if( nimi == "alpha" ) {
        mutex->lock();
        laskurit.alpha++;
        mutex->unlock();
    } else if( nimi == "beta") {
        mutex->lock();
        laskurit.beta++;
        mutex->unlock();
    } else if( nimi == "gamma" ) {
        mutex->lock();
        laskurit.gamma++;
        mutex->unlock();
    } else {} // jos ei tunneta, niin ei tehdä mitään
}

// Kerää näytteet säieturvallisesti rinnakkain
void parallelDetect(unsigned int j, std::mutex* mutex, std::mutex* mutex2)
{
    for(unsigned int i = 0; i < j; i++)
    {
        mutex->lock();
        auto nayte = rinn2017::detect();
        mutex->unlock();
        auto nimi = rinn2017::itemName( nayte );

        //Aiheuttaa virheellisiä printtauksia satunnaisesti säikeistyksen takia.
        mutex->lock();
        std::cout << "Luettiin " << nimi << std::endl;
        mutex->unlock();

        kirjaa_nayte( nayte, mutex2 );
    }
}


int main()
{
    // Tietorakenne, jonne uudet säikeet luodaan
    std::vector<std::thread> threads;
    // Lukko-olio
    // Meillä oli aluksi vain yksi mutex. Tehtiin kuitenkin lopulta
    // kaksi eri mutexia, toinen detect funktiolle ja toinen näytteiden kirjaamiselle.
    // Tämä sen takia, koska ajateltiin tämän nopeuttavan ohjelman toimintaa.
    // Detect ja keraa_nayte funktiot voivat toimia toisistaan erillään, niin saman mutexin
    // käyttö saattaa hidastaa ohjelman toimintaa lukkiutumisen takia.

    std::mutex* mutex = new std::mutex;
    std::mutex* mutex2 = new std::mutex;

    unsigned int naytteita_per_saie = NAYTTEITA/SAIKEITA;
    // Avaa vähintään 5 säiettä(vaikka 10)
    for( unsigned int i = 0; i < SAIKEITA; i++)
    {
        threads.push_back(std::thread(parallelDetect, naytteita_per_saie, mutex, mutex2));
    }

    // Odottaa, että kaikki säikeet on valmiita
    for(auto& thread : threads){
        thread.join();
    }

    // tulostetaan montako kappaletta kutakin näytettä saatiin
    std::cout << "Laskurit:" << std::endl;
    std::cout << "alpha: " << laskurit.alpha << std::endl;
    std::cout << "beta : " << laskurit.beta << std::endl;
    std::cout << "gamma: " << laskurit.gamma << std::endl;
    return EXIT_SUCCESS;
}
